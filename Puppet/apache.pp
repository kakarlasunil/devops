# == Class: to install apache
#
class apache {
  # resources
  apache::vhost {'example' :
  port => '80',
  docroot => '/var/www/html'
}
}
