provider "aws" {

region = "${var.region}"
}

resource "aws_instance" "scltest9001" {
  count = 1
  ami = "ami-2d39803a"
  instance_type = "t2.micro"
  vpc_id = "${var.aws_vpc}"
  subnet = "${var.subnet_pvr}"

  tags {
    Name = "scltest900${count.index+1}"
  }
}
